import ProjectDescription

let infoPlist: [String: Plist.Value] = [
    "CFBundleShortVersionString": "1.0",
    "CFBundleVersion": "1",
    "UIMainStoryboardFile": "",
    "UILaunchStoryboardName": "LaunchScreen",
]

let project = Project(
    name: "TuistShowcaseApp",
    targets: [
        Target(
            name: "TuistShowcaseApp",
            destinations: [.iPhone, .iPad],
            product: .app,
            bundleId: "engineering.towardsheadofmobile.TuistShowcaseApp",
            infoPlist: .extendingDefault(with: infoPlist),
            sources: ["TuistShowcaseApp/Sources/**"]
        ),
    ]
)
